#!/usr/bin/env bash
#
# Deploy resources to Azure using Azure Resource Manager templates.
#
# Required globals:
#   AZURE_APP_ID
#   AZURE_PASSWORD
#   AZURE_TENANT_ID
#   AZURE_RESOURCE_GROUP
#
# Optional globals:
#   AZURE_LOCATION
#   AZURE_DEPLOYMENT_NAME
#   AZURE_DEPLOYMENT_MODE
#   AZURE_DEPLOYMENT_NO_WAIT
#   AZURE_DEPLOYMENT_TEMPLATE_FILE
#   AZURE_DEPLOYMENT_TEMPLATE_URI
#   AZURE_DEPLOYMENT_PARAMETERS
#   AZURE_DEPLOYMENT_ROLLBACK_ON_ERROR
#   EXTRA_ARGS
#   DEBUG

source "$(dirname "$0")/common.sh"

enable_debug

# mandatory parameters
AZURE_APP_ID=${AZURE_APP_ID:?'AZURE_APP_ID variable missing.'}
AZURE_PASSWORD=${AZURE_PASSWORD:?'AZURE_PASSWORD variable missing.'}
AZURE_TENANT_ID=${AZURE_TENANT_ID:?'AZURE_TENANT_ID variable missing.'}
AZURE_RESOURCE_GROUP=${AZURE_RESOURCE_GROUP:?'AZURE_RESOURCE_GROUP variable missing.'}

debug AZURE_APP_ID: "${AZURE_APP_ID}"
debug AZURE_TENANT_ID: "${AZURE_TENANT_ID}"
debug AZURE_RESOURCE_GROUP: "${AZURE_RESOURCE_GROUP}"

# auth
AUTH_ARGS_STRING="--username ${AZURE_APP_ID} --password ${AZURE_PASSWORD} --tenant ${AZURE_TENANT_ID}"

if [[ "${DEBUG}" == "true" ]]; then
  AUTH_ARGS_STRING="${AUTH_ARGS_STRING} --debug"
fi

AUTH_ARGS_STRING="${AUTH_ARGS_STRING} ${EXTRA_ARGS:=""}"

debug AUTH_ARGS_STRING: "${AUTH_ARGS_STRING}"

info "Signing in..."

run az login --service-principal ${AUTH_ARGS_STRING}

# resource group
info "Ensuring Resource Group exists in Azure before deployment..."

AZURE_RESOURCE_GROUP_EXISTS=$(az group exists --name ${AZURE_RESOURCE_GROUP})

if [[ $AZURE_RESOURCE_GROUP_EXISTS == "false" ]]; then
  AZURE_LOCATION=${AZURE_LOCATION:?"'${AZURE_RESOURCE_GROUP}' Resource Group doesn't exist, but cannot be created because AZURE_LOCATION variable is missing."}

  GROUP_ARGS_STRING="--name ${AZURE_RESOURCE_GROUP} --location ${AZURE_LOCATION}"

  if [[ "${DEBUG}" == "true" ]]; then
    GROUP_ARGS_STRING="${GROUP_ARGS_STRING} --debug"
  fi

  GROUP_ARGS_STRING="${GROUP_ARGS_STRING} ${EXTRA_ARGS:=""}"

  debug GROUP_ARGS_STRING: "${GROUP_ARGS_STRING}"

  run az group create ${GROUP_ARGS_STRING}
fi

# deployment
ARGS_STRING="--resource-group ${AZURE_RESOURCE_GROUP}"

if [[ ! -z "${AZURE_DEPLOYMENT_NAME}" ]]; then
  ARGS_STRING="${ARGS_STRING} --name ${AZURE_DEPLOYMENT_NAME}"
fi

if [[ ! -z "${AZURE_DEPLOYMENT_MODE}" ]]; then
  ARGS_STRING="${ARGS_STRING} --mode ${AZURE_DEPLOYMENT_MODE}"
fi

if [[ ! -z "${AZURE_DEPLOYMENT_TEMPLATE_FILE}" ]]; then
  ARGS_STRING="${ARGS_STRING} --template-file ${AZURE_DEPLOYMENT_TEMPLATE_FILE}"
fi

if [[ ! -z "${AZURE_DEPLOYMENT_TEMPLATE_URI}" ]]; then
  ARGS_STRING="${ARGS_STRING} --template-uri ${AZURE_DEPLOYMENT_TEMPLATE_URI}"
fi

if [[ ! -z "${AZURE_DEPLOYMENT_PARAMETERS}" ]]; then
  ARGS_STRING="${ARGS_STRING} --parameters ${AZURE_DEPLOYMENT_PARAMETERS}"
fi

if [[ "${AZURE_DEPLOYMENT_NO_WAIT}" == "true" ]]; then
  ARGS_STRING="${ARGS_STRING} --no-wait"
fi

if [[ "${AZURE_DEPLOYMENT_ROLLBACK_ON_ERROR}" == "true" ]]; then
  ARGS_STRING="${ARGS_STRING} --rollback-on-error"
fi

if [[ "${DEBUG}" == "true" ]]; then
  ARGS_STRING="${ARGS_STRING} --debug"
fi

ARGS_STRING="${ARGS_STRING} ${EXTRA_ARGS:=""}"

debug ARGS_STRING: "${ARGS_STRING}"

info "Starting deployment to Resource Group..."

run az group deployment create ${ARGS_STRING}

if [ "${status}" -eq 0 ]; then
  success "Deployment successful."
else
  fail "Deployment failed."
fi