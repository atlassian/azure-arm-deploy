# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 1.1.2

- patch: Internal maintenance: Bump base docker image to 2.70.0.
- patch: Internal maintenance: Update pipes versions in pipelines configuration file.

## 1.1.1

- patch: Internal maintenance: Bump pipes versions in pipelines configuration file.
- patch: Internal maintenance: Update docker image to azure-cli:2.67.0

## 1.1.0

- minor: Bump azure-cli to version 2.63.0.
- patch: Internal maintenance: Bump release pipe version. Add multi-platform build: linux/arm64 and linux/amd64.

## 1.0.1

- patch: Internal maintenance: make pipe structure consistent to others.

## 1.0.0

- major: This pipe was forked from https://bitbucket.org/microsoft/azure-arm-deploy for future maintenance purpose.
